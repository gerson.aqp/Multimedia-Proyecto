﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;

public class movement : MonoBehaviour
{
    public GameObject Panel;
    public GameObject boton;
    public Text texto;
    public Text textoTitulo;
    public float m_moveSpeed = 5.0f;
    public float m_turnSpeed = 5.0f;
    private Animator m_animator;
    public float x, y;
    public static string url="nada";
    void Start()
    {
        m_animator = GetComponent<Animator>();
    }
    void Update()
    {
        float x = CrossPlatformInputManager.GetAxis("Horizontal");
        float y = CrossPlatformInputManager.GetAxis("Vertical");
        transform.Rotate(0, x * m_turnSpeed * Time.deltaTime, 0);
        transform.Translate(0, 0, y * m_moveSpeed * Time.deltaTime);

        m_animator.SetFloat("velX", x);
        m_animator.SetFloat("velY", y);
    }
    void OnTriggerEnter(Collider obj){
        boton.SetActive(true);
        switch (obj.tag)
        {
            case "manzana":
                textoTitulo.text = "Requerimientos del Tomate";
                texto.text= "\nA la hora de cultivar tomates en una maceta puedes hacerlo mediante " +
                    " semillas adquiridas o utilizando un tomate que tengas en el hogar. " +
                    "\nSe rellenan los semilleros con sustrato." +
                    "\nSe irán plantando uno a uno." +
                    "\nVuelves a añadir un poco de tierra (sustrato) por encima de cada semillero para procurar que la mayor parte del tallo y de la raíz queden dentro. " +
                    "\n¡Ya están listos! Ahora tendrás que mantener un buen cuidado de la planta y esperar a que crezcan. ";
                url = "https://goo.gl/maps/DqgL3ADBYcBvPb4t6";
                Panel.SetActive(true);
            break;
            case "cebolla":
                textoTitulo.text = "Requerimientos de la cebolla";
                texto.text = "\nLa idea es comenzar su cultivo partiendo de un bulbo de cebolla tal y como lo compramos en la tienda." +
                    "\nUbicar las cebollas plantadas en lugares cercanos a una buena fuente de luz. Con ello conseguiremos hojas más tersas y con mayor sabor.";
                url = "https://goo.gl/maps/DpoupcwUfWThyDzs5";
                Panel.SetActive(true);
                break;
            case "palta":
                textoTitulo.text = "Requerimientos de la palta";
                texto.text = "\nSacamos la pepita o hueso del aguacate." +
                    "\nLa limpiamos." +
                    "\nTenemos que introducir la mitad de la pepita o hueso en agua por lo que le ponemos tres palillos clavados en la mitad." +
                    "\nLlenamos un vaso de agua, no demasiado hondo, no es necesario." +
                    "\nColocamos la pepita o hueso con los palillos clavados." +
                    "\nPonemos más agua si es necesario hasta cubrir la mitad de la pepita." +
                    "\nEsperaremos a que salga la raíz, suele tardar unas 3 – 4 semanas." +
                    "\nCuando la raiz alcance unos 10 cm, sacaremos la pepita del agua." +
                    "\nLe quitamos los palillos." +
                    "\nPreparamos una maceta no muy grande con tierra." +
                    "\nPlantamos  y regamos la pepita. La colocaremos de tal forma que la mitad de ella esté dentro de la tierra y la otra mitad fuera." +
                    "\nEn poco más de un mes comenzaran a salirte hojas.";
                url = "https://g.page/semillera-montoya?share";
                Panel.SetActive(true);
                break;
            case "ajo":
                textoTitulo.text = "Requerimientos del ajo";
                texto.text = "\nLa semilla del ajo es el mismo diente de ajo, debemos seleccionar un ajo de buen tamaño, que no pase de 10 dientes por cabeza." +
                    "\n  Si nuestra semilla o diente tiene un tamaño grande, producirá un ajo de buen tamaño a la cosecha." +
                    "\n Al sembrar, debemos observar muy bien nuestro diente de ajo para no hacerlo al revés." +
                    "\n La punta o parte delgada es la parte superior, mientras que en la parte inferior podemos ver una pequeña superficie plana." +
                    "\n No se debe retirar la cáscara del diente de ajo." +
                    "\n La profundidad de siembra de nuestro ajo es el doble del tamaño del diente (5 cm aproximadamente)" +
                    "\n Aplicar un poco de composta al momento de la siembra y regar." +
                    "\n Puede colocar mulch para evitar que la humedad se vaya y crezcan malezas." +
                    "\n Los brotes de ajo comenzaran a salir aproximadamente entre 4-8 semanas, dependiendo del clima y la variedad.";
                url = "https://goo.gl/maps/7K8NAYEVBtB4dacb7";
                Panel.SetActive(true);
                break;
            case "brocoli":
                textoTitulo.text = "Requerimientos de la brocoli";
                texto.text = 
                    "\nLa distancia entre plantas es de 40cm." +
                    "\n Saque las plántulas con cuidado para no lastimar las raíces." +
                    "\n Si las raíces se encuentran muy enroscadas debe soltarlas y recortarlas un poco si es necesario, esto fomentará su crecimiento." +
                    "\n La profundidad del orificio del trasplante debe ser suficientemente profunda para que se cubra hasta las primeras hojas verdaderas." +
                    "\n Riegue sus plántulas después del trasplante." +
                    "\n Por sus nutritivas características, estos cultivos son el blanco perfecto para una gran cantidad de plagas." +
                    "\n La mejor manera de evitarlas  es monitoreando continuamente nuestras plantas" +
                    "\n Debemos revisar el envés de nuestras hojas ya que ahí podemos encontrar una gran cantidad de insectos como pulgones y larvas. ";
                url = "https://goo.gl/maps/LghynKj1fkcfkuvi6";
                Panel.SetActive(true);
                break;
            case "zanahoria":
                textoTitulo.text = "Requerimientos de la zanahoria";
                texto.text = 
                    "\n Realizar surcos de 1cm de profundidad y 8-10cm entre hileras." +
                    "\nPueden hacerse en hileras dobles o triples para aprovechar el  espacio." +
                    "\nColocar las semillas de zanahoria dejando 5cm entre ellas." +
                    "\nLas plántulas comenzarán a brotar a los 15-20 días de la siembra." +
                    "\nLos cotiledones serán hojas delgadas y alargadas." +
                    "\nCuando aparezca el segundo par de hojas debemos colocar suelo, mezcla preparada con composta o sólo composta, hasta la base del primer par de hojas." +
                     "\nDeshierbar para evitar plagas y enfermedades." +
                    "\nRetirar las hojas viejas y amarillas de las plantas.";
                url = "https://g.page/HORTUS-AREQUIPA?share";
                Panel.SetActive(true);
                break;
            case "limones":
                textoTitulo.text = "Requerimientos del limon";
                texto.text = 
                    "\n Comienza apartando las semillas de un limón fresco y recién cortado." +
                    "\nDespués, toma un pedazo de servitoalla o papel de cocina y humedécelo con agua templada." +
                    "\nColoca las semillas del limón sobre el papel y cúbrelas con otro papel húmedo." +
                    "\nDeja pasar los días hasta que comience a germinar." +
                    "\nRecuerda hidratar el papel todos los días con pequeñas cantidades de agua" +
                    "\nUtiliza un atomizador para tener más control y cuida que el papel no se rompa." +
                    "\nCuando tus semillas germinen deberás trasplantar a un lugar permanente.";
                url = "https://goo.gl/maps/HssD3b3sU7ETn7Pb7";
                Panel.SetActive(true);
                break;
            case "pepino":
                textoTitulo.text = "Requerimientos del pepino";
                texto.text = 
                    "\n Las semillas de pepino no germinan hasta que la temperatura del suelo alcance unos 15 o 16 grados centígrados." +
                    "\nGeneralmente los pepinos se cultivan en primavera." +
                    "\nSi vives en una zona donde el clima es más fresco te aconsejamos que esperes un poco hasta que el frío amaine o los siembres en semillero en interior hasta que la temperatura del suelo suba unos grados." +
                    "\n A los pepinos no les gustan los suelos encharcados por diferentes motivos. Uno de ellos es la fácil aparición y propagación de plagas como el oídio y el mildiu." +
                    "\n A los pepinos les gusta el calor, pero las temperaturas demasiado altas provocarán que salgan más flores macho que hembras, provocando una disminución de la producción.";
                url = "https://goo.gl/maps/ejmiQN3S9iinEUf36";
                Panel.SetActive(true);
                break;
            case "calabacin":
                textoTitulo.text = "Requerimientos del calabacin";
                texto.text = 
                    "\n l calabacín es un cultivo que necesita un clima cálido para germinar." +
                    "\nPara cultivar el calabacín en tu huerto en casa, puedes hacer la siembra" +
                    "directamente en tierra o en una capa de arena, a razón de 2-3 semillas por hoyo" +
                    ", a una profundidad de 2 cms. Las semillas se siembran juntas porque el objetivo es que al emerger rompan la costra del suelo con mayor facilidad." +
                    "\nUna vez colocadas, hay que cubrirlas con 3-4 cms. de tierra o arena según corresponda." +
                    "\nUna vez que se haya producido la germinación, se recomienda dejar crecer sólo un plantín por hoyo." +
                    "\nPara ello se debe elegir la más fuerte y descartar las otras. La época de siembra es de marzo a septiembre." +
                    "\n" +
                    "\nSi plantas el calabacín junto a la capuchina, alejará a los pulgones y a las chinches, y mejorará su sabor; es una planta muy beneficiosa para este cultivo. ";
                url = "https://goo.gl/maps/cgNfJNq3G3gJzo9v7";
                Panel.SetActive(true);
                break;
        }

    }
    void OnTriggerExit(Collider obj)
    {
        Panel.SetActive(false);
        boton.SetActive(false);
    }

}
